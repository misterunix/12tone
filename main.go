package main

import (
	"fmt"

	"bitbucket.org/misterunix/godice"
)

type tonearray struct {
	notes     [12][12]int
	intervals [12][12]int
}

func main() {

	t := tonearray{}
	for i := 0; i < 12; i++ {
		t.notes[0][i] = i
	}

	for i := 0; i < 144; i++ {
		l1 := godice.Roll(1, 12) - 1
		l2 := godice.Roll(1, 12) - 1
		v1 := t.notes[0][l1]
		v2 := t.notes[0][l2]
		t.notes[0][l1] = v2
		t.notes[0][l2] = v1
	}

	t.intervals[0][0] = 0
	for x := 1; x < 12; x++ {
		k := t.notes[0][x] - t.notes[0][0]
		if k < 11 {
			k = k + 12
		}
		if k > 11 {
			k = k - 12
		}
		t.intervals[0][x] = k
	}

	for x := 0; x < 12; x++ {
		for y := 0; y < 12; y++ {
			k := t.notes[0][0] + t.intervals[0][x]
			if k > 11 {
				k = k - 12
			}
			if k < 11 {
				k = k + 12
			}
			t.notes[y][x] = k
		}
	}

	// Print Matrix
	for y := 0; y < 12; y++ {
		for x := 0; x < 12; x++ {
			fmt.Printf("%02d ", t.notes[y][x])
		}
		fmt.Println()
	}
	fmt.Println()
	fmt.Println()
	fmt.Println()
	for y := 0; y < 12; y++ {
		for x := 0; x < 12; x++ {
			fmt.Printf("%02d ", t.intervals[y][x])
		}
		fmt.Println()
	}
	fmt.Println()

	fmt.Println(t.notes[0])
	fmt.Println(t.intervals[0])
}
