package main

type ChromaticData struct {
	Root            int
	Notes           [50]int
	Chromatics      [4][12]string
	ChromaticNumber int
}

func NewChromatic() ChromaticData {
	d := ChromaticData{}

	d.Chromatics = [4][12]string{
		{"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"},
		{"C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"},
		{"C", "C#", "D", "D#", "E", "E#", "F#", "G", "G#", "A", "A#", "B"},
		{"C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "Cb"},
	}

	return d
}

